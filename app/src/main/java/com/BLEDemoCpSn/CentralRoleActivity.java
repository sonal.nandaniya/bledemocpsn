package com.BLEDemoCpSn;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class CentralRoleActivity extends BluetoothActivity implements View.OnClickListener, DevicesAdapter.DevicesAdapterListener {


    /**
     * Scanning will be stop after 30 seconds.
     */
    private static final long SCAN_PERIOD_TIMEOUT = 30000;

    private RecyclerView mDevicesRecycler;
    private DevicesAdapter mDevicesAdapter;
    private Button mScanButton;
    private ScanCallback mScanCallback;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 2;

    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScanButton = (Button) findViewById(R.id.button_scan);
        mScanButton.setOnClickListener(this);
        mDevicesRecycler = (RecyclerView) findViewById(R.id.devices_recycler_view);
        mDevicesRecycler.setHasFixedSize(true);
        mDevicesRecycler.setLayoutManager(new LinearLayoutManager(this));

        mDevicesAdapter = new DevicesAdapter(this);
        mDevicesRecycler.setAdapter(mDevicesAdapter);

        mHandler = new Handler(Looper.getMainLooper());

    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_central_role;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.button_scan:
                startBLEScan();
                break;

        }
    }


    @Override
    protected int getTitleString() {
        return R.string.central_screen;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ENABLE_BT:

                if (resultCode == RESULT_OK) {

                    initBT();

                } else {

                    // User declined to enable Bluetooth, exit the app.
                    Toast.makeText(this, R.string.bt_not_enabled, Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void startBLEScan() {

        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

        /*
        better to request each time as BluetoothAdapter state might change (connection lost, etc...)
         */
        if (bluetoothAdapter != null) {

            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

            if (bluetoothLeScanner != null) {
                if (bluetoothAdapter.isEnabled()) {

                    if (mScanCallback == null) {
                        Log.d(MainActivity.TAG, "Starting Scanning");

                        // Will stop the scanning after a set time.
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                stopScanning();
                            }
                        }, SCAN_PERIOD_TIMEOUT);

                        mScanCallback = new SampleScanCallback();
                        bluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);

                        String toastText =
                                getString(R.string.scan_start_toast) + " "
                                        + TimeUnit.SECONDS.convert(SCAN_PERIOD_TIMEOUT, TimeUnit.MILLISECONDS) + " "
                                        + getString(R.string.seconds);

                        showMsgText(toastText);

                    } else {
                        showMsgText(R.string.already_scanning);
                    }

                    return;
                }
            } else {
                Intent enableBtIntent1 = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent1, REQUEST_ENABLE_BT);
            }
        } else {
            Intent enableBtIntent1 = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent1, REQUEST_ENABLE_BT);
        }
        //showMsgText(R.string.error_unknown);
    }

    /**
     * Return a List of {@link ScanFilter} objects to filter by Service UUID.
     */
    private List<ScanFilter> buildScanFilters() {

        List<ScanFilter> scanFilters = new ArrayList<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        // Comment out the below line to see all BLE devices around you
        //builder.setServiceUuid(Constants.SERVICE_UUID);
        scanFilters.add(builder.build());

        return scanFilters;
    }

    /**
     * set to use low power (to preserve battery life).
     */
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        return builder.build();
    }


    public void stopScanning() {

        Log.d(MainActivity.TAG, "Stopping Scanning");

        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

        if (bluetoothAdapter != null) {

            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

            if (bluetoothLeScanner != null) {

                bluetoothLeScanner.stopScan(mScanCallback);
                mScanCallback = null;
                mDevicesAdapter.notifyDataSetChanged();

                return;
            }
        }

        showMsgText(R.string.error_unknown);
    }


    @Override
    public void onDeviceItemClick(String deviceName, String deviceAddress) {

        Intent intent = new Intent(this, DeviceConnectActivity.class);
        intent.putExtra(DeviceConnectActivity.EXTRAS_DEVICE_NAME, deviceName);
        intent.putExtra(DeviceConnectActivity.EXTRAS_DEVICE_ADDRESS, deviceAddress);
        startActivity(intent);
    }


    private class SampleScanCallback extends ScanCallback {

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            mDevicesAdapter.add(results);
            logResults(results);
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            mDevicesAdapter.add(result);
            logResults(result);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            showMsgText("Scan failed with error: " + errorCode);
        }


        private void logResults(List<ScanResult> results) {
            if (results != null) {
                for (ScanResult result : results) {
                    logResults(result);
                }
            }
        }

        private void logResults(ScanResult result) {
            if (result != null) {
                BluetoothDevice device = result.getDevice();
                if (device != null) {
                    Log.v(MainActivity.TAG, device.getName() + " " + device.getAddress());
                    return;
                }
            }
            Log.e(MainActivity.TAG, "error SampleScanCallback");
        }
    }


    private void initBT() {

        BluetoothManager bluetoothService = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE));

        if (bluetoothService != null) {

            BluetoothAdapter mBluetoothAdapter = bluetoothService.getAdapter();

            // Is Bluetooth supported on this device?
            if (mBluetoothAdapter != null) {

                // Is Bluetooth turned on?
                if (mBluetoothAdapter.isEnabled()) {

                    // Are Bluetooth Advertisements supported on this device?
                    if (mBluetoothAdapter.isMultipleAdvertisementSupported()) {

                        // see https://stackoverflow.com/a/37015725/1869297
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                            } else {
                                // Everything is supported and enabled.
                                startBLEScan();
                            }

                        } else {
                            // Everything is supported and enabled.
                            startBLEScan();
                        }


                    } else {

                        // Bluetooth Advertisements are not supported.
                        showErrorText(R.string.bt_ads_not_supported);
                    }
                } else {

                    // Prompt user to turn on Bluetooth (logic continues in onActivityResult()).
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
            } else {

                // Bluetooth is not supported.
                showErrorText(R.string.bt_not_supported);
            }

        }
    }

    private void showErrorText(int string) {
        Snackbar snackbar = Snackbar.make(mScanButton, string, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_REQUEST_COARSE_LOCATION:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorText(R.string.bt_not_permit_coarse);
                } else {
                    // Everything is supported and enabled.
                    startBLEScan();
                }
                break;

        }
    }

}
